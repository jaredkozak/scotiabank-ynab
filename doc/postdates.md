Scotiabank
icon_timeA chat representative will be with you shortly. Thank you for waiting.
Chat representative Dexter E has joined the session and is ready to help. To start, enter a message below.
Scotiabank AgentDexter E: 4:12 PMThank you for choosing Scotiabank Canada’s Live Chat. My name is Dexter, how may I help you today?
CustomerJARED: 4:12 PMHi Dexter,
CustomerJARED: 4:13 PMI'm trying to access the transaction date of specific transactions for my credit card on the website. These transactions are recently posted so there is no statement for them. How do I access the transaction date from the website? Not the app
Scotiabank AgentDexter E: 4:14 PMHello Jared, you will have to wait for the transactions to be posted to see the date of the transactions.
CustomerJARED: 4:15 PMAfter they've been posted, how do I see the transaction date? (not the posted date) - the website shows the posted date only
Scotiabank AgentDexter E: 4:16 PMIt will show you transactions date right above the transactions for that day.
CustomerJARED: 4:18 PMOh, that IS the transaction date?
CustomerJARED: 4:19 PMI'm seeing a table of:
CustomerJARED: 4:19 PMThe date shown in this account activity for a posted transaction tells you when the transaction happened (transaction date), not when it was posted to your account. On your paper or e-statement, you’ll see a transaction date (when the transaction happened) and a post date (when the transaction was posted to your account).
CustomerJARED: 4:19 PMDate | Transaction Description | Debits | credits
CustomerJARED: 4:19 PMFor my Visa Infinite
CustomerJARED: 4:19 PMUnder "Posted Transactiions"
CustomerJARED: 4:19 PMThe "Date" shown is the transaction date?
Scotiabank AgentDexter E: 4:21 PMI do apologise Jared that would be the posted date, the transaction dates are only available once you get the e-statements. You will only see the posted date on your online banking.
Scotiabank AgentDexter E: 4:22 PMYou will have to wait until you get your e-statement to get the transaction date on your transactions unfortunately.
CustomerJARED: 4:23 PMInteresting, so the help text is incorrect then?
CustomerJARED: 4:23 PMThe date shown in this account activity for a posted transaction tells you when the transaction happened (transaction date), not when it was posted to your account. On your paper or e-statement, you’ll see a transaction date (when the transaction happened) and a post date (when the transaction was posted to your account).
CustomerJARED: 4:24 PMThat would make sense, because I made a transaction on Aug 29 but then today it is showing up as posted, aug 30
Scotiabank AgentDexter E: 4:24 PMYou are correct on your e-statement will show you the transaction date and the posted date.
CustomerJARED: 4:25 PMBut on the website it shows me the transaction date or the posted date?
Scotiabank AgentDexter E: 4:27 PMJared, let me make absolutely show give me one moment please.
CustomerJARED: 4:27 PMFor sure! I appreciate the diligence
Scotiabank AgentDexter E: 4:32 PMThank you for the wait and your patience. The date that you see on the website next the transaction is the transaction date. On the e-statement it shows you the posted date as well and in the scotia app you can click on the transaction to see both the posted and transaction date.
Scotiabank AgentDexter E: 4:32 PM*to*
CustomerJARED: 4:33 PMAwesome! Thank you!
Scotiabank AgentDexter E: 4:33 PMIt can take up to 3 business days for some transactions to be posted. With your skip the dishes transaction it posted on the same day which was August 30th.
CustomerJARED: 4:33 PMI have another, similar question about that transaction
Scotiabank AgentDexter E: 4:34 PMAbsolutely go right ahead.
CustomerJARED: 4:34 PMI made it on aug 29 at 10:29PM - I immediately saw it in my list of pending transactions and it was listed as "aug 29" - but now it's listed as aug 30. Do you know why? Is there something odd going on with timezones?
Scotiabank AgentDexter E: 4:37 PMJared, that is a great question. I do believe it could have been a glitch in the system because whatever timezone the transaction was made in it should show that time.
Scotiabank AgentDexter E: 4:37 PMHas this happened before on your account?
CustomerJARED: 4:39 PMThis is the first time I've seen it; I'm wondering if maybe the pending transactions are showing the transaction in one timezone while the posted transactions are showing another? That would make sense if it was in eastern time or something
CustomerJARED: 4:39 PMOr if the date is rounding because it was later? I'm not sure
Scotiabank AgentDexter E: 4:41 PMI believe the better answer is it's because the transaction was done late it rounded to the next day. Jared, this is definitely something I will look into because I have never heard of this scenario before.
Scotiabank AgentDexter E: 4:41 PMI can definitely reach out to my consultant and have him take a look into this then get back to you to make sure.
CustomerJARED: 4:42 PMAmazing! I would really appreciate that
Scotiabank AgentDexter E: 4:43 PMRight now they are all busy do you have a number that I can call you back with?
CustomerJARED: 4:43 PMyes here is my cell: <redacted>
Scotiabank AgentDexter E: 4:44 PMThank you for that information, Jared you can be expecting call within 15-30 minutes. Do you have anymore questions or concerns for me today?
CustomerJARED: 4:45 PMThank you! No more questions, I really appreciate the diligence & followup. I realize these questions were not typical! Thank you!
Scotiabank AgentDexter E: 4:45 PMIt was a pleasure chatting with you today. Thank you for choosing Scotiabank’s Live Chat, have an excellent day.
Dexter E has left the chat