module.exports = {
    'env': {
        'browser': true,
        'es2021': true
    },
    'extends': [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended'
    ],
    'overrides': [
    ],
    'parser': '@typescript-eslint/parser',
    'parserOptions': {
        'ecmaVersion': 'latest',
        'sourceType': 'module'
    },
    'plugins': [
        '@typescript-eslint'
    ],
    'rules': {
        'indent': [
            'error',
            4
        ],
        'linebreak-style': [
            'error',
            'unix'
        ],
        'quotes': [
            'error',
            'single',
            {
                allowTemplateLiterals: true,
            }
        ],
        'prefer-const': [
            'error', {
                destructuring: 'all',
            }
        ],
        '@typescript-eslint/ban-ts-comment': [
            0
        ],
        "no-unused-vars": [
            "warn", {
                // "vars": "all",
                // "args": "after-used",
                // "ignoreRestSiblings": false
            }],
        'semi': [
            'error',
            'always'
        ]
    }
};
