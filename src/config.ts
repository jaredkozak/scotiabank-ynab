import { environment, UNDEFINED_STRING } from '@kozakatak/environment-loader';
import * as dotenv from 'dotenv';
import { MS_ONE_HOUR } from './time';

export let cfg: ReturnType<typeof initConfig>;

const defaultEnvironment = {
    logLevel: 'debug',

    interval: MS_ONE_HOUR,
    // interval: ,
    headless: true,
    dry: false,
    closeAfterSync: true,

    minSyncHours: 9,
    maxSyncHours: 18,
    disablePendingTransactions: true,
    
    scotiabank: {
        username: UNDEFINED_STRING,
        password: UNDEFINED_STRING,
        timezone: 'America/Toronto',
    },

    ynab: {
        accessToken: UNDEFINED_STRING,
        budget: UNDEFINED_STRING,
    },

    browserDataDirectory: './user_data',



    // https://stackoverflow.com/questions/61443364/how-to-make-selenium-headless-undetectable
    browserUserAgent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.101 Safari/537.36',
};

export function initConfig() {
    dotenv.config({});

    const env = environment(defaultEnvironment, {
        // logger,
        prefix: 'app',
    });

    cfg = env;

    return env;
}
