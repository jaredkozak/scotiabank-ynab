
export function currentTimeSeconds() {
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
    return Math.round(Date.now() / 1000);
}

export const MS_ONE_SECOND = 1000;
export const MS_ONE_HOUR = 3600000;
export const MS_ONE_DAY = 86400000;
export const MS_ONE_WEEK = 604800000;
export const MS_ONE_MONTH = 2592000000;
export const MS_ONE_YEAR = 31536000000;

export const S_ONE_MINUTE = 60;
export const S_ONE_HOUR = 3600;
export const S_THREE_HOURS = 10800;
export const S_ONE_DAY = 86400;
export const S_ONE_WEEK = 604800;
export const S_FOUR_WEEKS = 2419200;
export const S_ONE_YEAR = 31536000;
export const S_TWO_YEARS = 63072000;
export const S_TEN_YEARS = 3.154e+8;

export const HOURS_TO_SECONDS = 3600;
// eslint-disable-next-line @typescript-eslint/no-magic-numbers
export const UNIX_MS_S_THRESHOLD = currentTimeSeconds() * 10;

export enum MONTH_INDEX {
    JANUARY,
    FEBRURARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER,
}

/**
 * Divides val by 1000 if val is greater than UNIX_MS_S_THRESHOLD
 * @returns integer
 */
export function ensureUnixSeconds(val?: number) {
    if (val === undefined || val === null) { return val; }
    if (val > UNIX_MS_S_THRESHOLD) {
        return Math.round(val / MS_ONE_SECOND);
    }
    return val;
}

export async function resolveMilliseconds(milliseconds: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, milliseconds);
    });
}

export async function resolveNever() {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    return new Promise(() => {});
}
