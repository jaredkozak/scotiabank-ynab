import winston from 'winston';
import { cfg } from './config';

export let logger = console as unknown as winston.Logger;

export function initLogger() {

    logger = winston.createLogger({
        level: cfg.logLevel,
        // format: winston.format.json(),
        format: winston.format.combine(
            winston.format.colorize(),
            winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
            // winston.format.padLevels(),
            // winston.format.json(),
            winston.format.printf(info => {
                let message = typeof info.message === 'object' ? JSON.stringify(info.message, null, 2) : info.message;
                const messages = [ message ];

                // @ts-ignore splat
                const splat = info[Symbol.for('splat')];
                if (splat) {
                    for (const m of splat) {
                        messages.push(typeof m === 'object' ? JSON.stringify(m, null, 2) : m);
                    }
                }
                message = messages.join(' ');

                return `${info.timestamp} ${info.level}: ${message}`;
            })

        ),
        // defaultMeta: {  },
        transports: [
            //
            // - Write all logs with importance level of `error` or less to `error.log`
            // - Write all logs with importance level of `info` or less to `combined.log`
            //
            //   new winston.transports.File({ filename: 'error.log', level: 'error' }),
            //   new winston.transports.File({ filename: 'combined.log' }),
            new winston.transports.Console({
                // format: winston.format.combine(
                //     // winston.format.colorize(),
                //     winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
                //     winston.format.prettyPrint()
                // ),
            }),
        ],
    });

    // Object.assign(logger, _logger);

    logger.debug(`Logger initialized.`);
}
