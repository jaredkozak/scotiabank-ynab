import { Browser, Builder, WebDriver } from 'selenium-webdriver';
import { Options as ChromeOptions } from 'selenium-webdriver/chrome';
import { cfg } from './config';

export let webDriver: WebDriver;
export let open = false;

export async function initSelenium() {
    if (webDriver || open) { return; }

    // new ChromiumWebDriver()

    // https://stackoverflow.com/questions/53039551/selenium-webdriver-modifying-navigator-webdriver-flag-to-prevent-selenium-detec

    const options = new ChromeOptions();
    // options.setChromeBinaryPath('/opt/brave.com/brave/brave-browser');
    const args = [
        '--disable-extensions',
        '--disable-gpu',
        // '--no-sandbox',
        '--disable-blink-features=AutomationControlled',
        `--user-agent="${ cfg.browserUserAgent }"`,
        `user-data-dir=${ cfg.browserDataDirectory }`,
        // '--start-maximized',
        // '--',
    ];
    if (cfg.headless) {
        args.push('--headless');
    }

    options.addArguments(
        ...args,
    );

    options.excludeSwitches('enable-automation', 'enable-logging');
    // options.useaut

    webDriver = await new Builder()
        .forBrowser(Browser.CHROME)
        // .setFirefoxOptions(options)
        .setChromeOptions(options)
        .build();

    return webDriver;

}

export async function closeSelenium() {
    await webDriver.close();
    // webDriver = undefined as unknown as WebDriver;
    open = false;
}


export async function quitSelenium() {
    if (webDriver) {
        await webDriver.quit();
    }

    webDriver = undefined as unknown as WebDriver;
    open = false;
}

