
import dayjs_import from 'dayjs';

import duration from 'dayjs/plugin/duration';
import relativeTime from 'dayjs/plugin/relativeTime';
dayjs_import.extend(duration);
dayjs_import.extend(relativeTime);

// dayjs_import.extend();

// export const dayjs = dayjs_import;
// export type Dayjs = typeof dayjs;
