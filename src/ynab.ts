
import { Account, API, BudgetSummary } from 'ynab';
import { BankAccount } from './app';
import { cfg } from './config';
import { logger } from './logger';

export let ynab: API;
export let budget: BudgetSummary;

export const YNAB_DATE_FORMAT = 'YYYY-MM-DD';

export async function initYNAB() {

    ynab = new API(cfg.ynab.accessToken);

    const { data: { user } } = await ynab.user.getUser();
    logger.info(`YNAB authenticated with user ID: ${ user.id }`);

    const { data: { budgets } } = await ynab.budgets.getBudgets();

    logger.info(`Loaded ${ budgets.length } budgets from YNAB`);

    if (!budgets?.length) {
        throw new Error(`Cannot initialize YNAB: no budgets found in ynab account`);
    }

    budget = budgets[0];
    // TODO: use default budget if set

    if (cfg.ynab.budget) {
        const foundBudget = budgets.find((b) => b.id === cfg.ynab.budget);
        if (!foundBudget) {
            throw new Error(`Cannot initialize YNAB: cannot find budget with ID ${ cfg.ynab.budget }`);
        }
        budget = foundBudget;
    }


    return ynab;
}

export function getAccountByLink(
    accounts: Account[],
    name: string,
) {
    const nameFixed = name.replace(/ /g, '').toLowerCase();
    return accounts.find((a) => {
        const note = a.note;
        if (!note) { return false; }
        const split = note.toLowerCase().split('\n');
        const link = split.find((s) => s.startsWith('link:'));
        if (!link) { return false; }
        let [ , noteName ] = link.split(':');
        noteName = noteName.replace(/ /g, '').toLowerCase();
        logger.info(noteName);
        if (!noteName) { return; }
        if (nameFixed.startsWith(noteName)) {
            return true;
        }

        return false;
    });
}


export function getAccountLink(
    account: Account,
    bankAccounts: BankAccount[],
) {
    const note = account.note;
    if (!note) { return false; }
    const split = note.toLowerCase().split('\n');
    const link = split.find((s) => s.startsWith('link:'));
    if (!link) { return false; }
    let [ , noteName ] = link.split(':');
    noteName = noteName.replace(/ /g, '').toLowerCase();

    return bankAccounts.find((a) => {
        const nameFixed = a.name.replace(/ /g, '').toLowerCase();
        return nameFixed.startsWith(noteName);
    });
}
