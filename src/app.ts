import dayjs, { Dayjs } from 'dayjs';
import { By, Key, until, WebElement } from 'selenium-webdriver';
import { Account, SaveTransaction, TransactionDetail, UpdateTransaction } from 'ynab';
import { cfg } from './config';
import { logger } from './logger';
import { initSelenium, quitSelenium, webDriver } from './selenium';
import { resolveMilliseconds } from './time';
import { budget, getAccountLink, ynab, YNAB_DATE_FORMAT } from './ynab';

const home = `https://www.scotiabank.com/ca/en/personal.html`;
const accountSummary = `https://www.scotiaonline.scotiabank.com/online/views/accounts/summary/summaryStandard.bns?SBL=all&convid=167526`;

export async function getLowercaseTitle() {
    return (await webDriver.getTitle()).toLowerCase();
}

export async function setup() {
    logger.info(`Running setup`);
    await webDriver.get(accountSummary);
    let title = await getLowercaseTitle();
    if (title.includes('account summary')) {
        return;
    } else if (!title.includes('sign in')) {
        await webDriver.get('https://www.scotiabank.com/ca/en/personal.html');
        await webDriver.findElement(By.css('.btn-signin')).click();
    }

    // TODO: login logic

    // await webDriver.get('https://www.scotiaonline.scotiabank.com/');
    await resolveMilliseconds(1000);

    const [usernameElement] = await webDriver.findElements(By.id('usernameInput-input'));
    if (usernameElement) {
        logger.info(`Entering Username`);
        usernameElement.sendKeys(cfg.scotiabank.username);
    } else {
        logger.info(`Username is prefilled`);
        await webDriver.findElement(By.id('dropdownValue'));
    }
    await resolveMilliseconds(10);

    // await resolveMilliseconds(1000);
    logger.info(`Entering Password`);
    const password = await webDriver.findElement(By.id('password-input'));
    await password.sendKeys(cfg.scotiabank.password);
    logger.info(`Password entered`);
    await resolveMilliseconds(10);

    const [rememberMe] = await webDriver.findElements(By.css('.Checkbox__container'));
    if (rememberMe) {
        rememberMe.click();
        logger.info(`Remembering username for next time`);
    }

    await resolveMilliseconds(1000);
    await password.sendKeys(Key.RETURN);
    await resolveMilliseconds(3000);


    // await resolveMilliseconds(600);
    // const element = await webDriver.findElement()
    // await webDriver.findElement(By.id('signIn')).click();

    title = await getLowercaseTitle();
    logger.info(`Post 2FA title: ${title}`);
    if (title.includes('something went wrong')) {
        throw new Error(`Cannot authenticate: ${ title }`);
    }

    if (title.includes('go to your scotiabank app')) {
        logger.info(`Waiting for 2fa...`);
        // wait for user to enable 2fa
        await webDriver.wait(until.titleContains('Sign in confirmed'));
        const [twoFactorRememberMe] = await webDriver.findElements(By.css('.Checkbox__container'));
        if (twoFactorRememberMe) {
            twoFactorRememberMe.click();
            logger.info(`Remembering for next time`);
            await webDriver.findElement(By.id('continue')).click();
        }
    } else {
        logger.info(`2FA is not required.`);
    }


    // TODO: error logic

}

export interface BankAccount {
    type: 'banking' | 'investment' | 'borrowing';
    name: string;
    url: string;
    balance: number;
    currency: string;
    linkId: string;
}

export interface BankTransaction {
    date: Dayjs;
    description: string;

    cleared: boolean;

    amount: number;
    debit?: number;
    credit?: number;
    balance?: number;

    image?: string;
    imageAlt?: string;
}

export interface AccountInfo {
    account: BankAccount;
    latestTransactions: BankTransaction[];
}

async function getAccountInfoFromTableRow(
    row: WebElement,
    accountType: string,
) {

    const rowId = await row.getAttribute('id');
    if (!rowId) { return; }

    const [anchor] = await row.findElements(By.css('td:first-child a'));
    if (!anchor) { return; }
    // logger.info(anchor.)
    const url = await anchor.getAttribute('href');
    const linkId = await anchor.getAttribute('id');
    const name = await anchor.getText();
    const [balanceDiv] = await row.findElements(By.css('td.balance div.number'));
    if (!balanceDiv) { return; }
    const strBalance = await balanceDiv.getText();
    const balance = formatBalance(strBalance);

    return {
        type: accountType,
        balance,
        name,
        url,
        linkId,
        currency: 'CAD',
    } as BankAccount;
}

export async function ensureURL(
    url: string,
    titleCheck?: string,
    timeout = 300,
) {

    if (titleCheck) {
        titleCheck = titleCheck.toLowerCase();
    }

    logger.debug(`Ensuring url: ${url}`);
    let currentUrl = await webDriver.getCurrentUrl();
    if (currentUrl === url) {
        logger.debug(`URL already matched`);
        return;
    }

    let currentTitle = await getLowercaseTitle();
    if (titleCheck && currentTitle.includes(titleCheck)) {
        logger.debug(`Title matches`);
        return;
    }

    await webDriver.get(url);
    await resolveMilliseconds(timeout);
    currentUrl = await webDriver.getCurrentUrl();
    if (currentUrl === url) {
        logger.debug(`URL matches immediately`);
        return;
    }

    currentTitle = await getLowercaseTitle();
    if (titleCheck && currentTitle.includes(titleCheck)) {
        logger.debug(`Title matches immediately`);
        return;
    }

    if (currentTitle.includes('sign in')) {
        await setup();
    }

    currentTitle = await getLowercaseTitle();
    if (titleCheck && currentTitle.includes(titleCheck)) {
        logger.debug(`Title matches after sign in`);
        return;
    }

    await webDriver.get(url);
    await resolveMilliseconds(timeout);
    currentUrl = await webDriver.getCurrentUrl();
    if (currentUrl === url) {
        logger.debug(`URL matches after sign in`);
        return;
    }
    currentTitle = await getLowercaseTitle();
    if (titleCheck && currentTitle.includes(titleCheck)) {
        logger.debug(`Title matches after sign in and route`);
        return;
    }


    logger.debug(`Cannot set url; title: ${currentTitle} url: ${currentTitle} titleCheck: ${titleCheck}`);
    throw new Error(`Cannot set url: ${url} from ${currentUrl}`);

}

export async function getAccountList(
    summaryKey: string,
    accountType: string,
) {
    await ensureURL(accountSummary);

    const bankingAccountRows = await webDriver.findElements(By.css(`table[summary="${summaryKey}"] tbody tr`));
    const accounts: BankAccount[] = [];
    for (const row of bankingAccountRows) {
        const account = await getAccountInfoFromTableRow(row, accountType);
        if (!account) { continue; }
        accounts.push(account);
    }

    return accounts;

}

export async function getAllAccounts() {
    return [
        ...(await getAccountList('Banking accounts', 'banking')),
        ...(await getAccountList('Investment accounts', 'investment')),
        ...(await getAccountList('Borrowing accounts', 'borrowing')),
    ];
}

export function formatBalance(str: string) {
    return Number(str.replace(/[,]/, ''));
}

export async function processBorrowingTransactionRow(
    row: WebElement,
    cleared: boolean,
) {
    const [cDate, , cDescription, cDebit, cCredit] = await row.findElements(By.css('th, td'));

    if (!cDate || !cDescription) { return; }
    const date = await cDate.getText();
    if (!date) { return; }


    const debit = formatBalance(await cDebit.getText());
    const credit = formatBalance(await cCredit.getText());
    const amount = credit || (debit ? -debit : 0);

    return {
        date: dayjs(date, `MMM. DD, YYYY`),
        description: await cDescription.getText(),
        cleared,
        credit,
        debit,
        amount,
        // balance,
        // image,
        // imageAlt,
    } as BankTransaction;
}

export async function getTransactionsFromTable(
    summaryKey: string,
    cleared: boolean,
    func: (row: WebElement, cleared: boolean) => Promise<BankTransaction | undefined>,
) {
    const txRows = await webDriver.findElements(By.css(`table[summary="${summaryKey}"] tbody tr`));
    if (!txRows?.length) { return []; }

    const arrTx = (await Promise.all(
        txRows
            .map(async (row) => func(row, cleared))
    )).filter(Boolean) as BankTransaction[];

    return arrTx;
}

export async function processBankingTransactionRow(
    row: WebElement,
    cleared: boolean,
) {
    const [cDate, cDescription, cDebit, cCredit, cBalance ] = await row.findElements(By.css('th, td'));

    if (!cDate || !cDescription) { return; }
    const date = await cDate.getText();
    if (!date) { return; }


    const debit = formatBalance(await cDebit.getText());
    const credit = formatBalance(await cCredit.getText());
    const amount = credit || (debit ? -debit : 0);
    const balance = formatBalance(await cBalance.getText());

    return {
        date: dayjs(date, `MMM. DD, YYYY`),
        description: await cDescription.getText(),
        cleared,
        credit,
        debit,
        amount,
        balance,
        // image,
        // imageAlt,
    } as BankTransaction;
}

export async function getBorrowingTransactions(
    account: BankAccount,
) {
    let pendingTransactions = await getTransactionsFromTable('Pending Transactions', false, processBorrowingTransactionRow);

    // remove transactions from the current day
    pendingTransactions = pendingTransactions.filter((tx) => {
        return tx.date.isBefore(dayjs().startOf('day'));
    });
    const transactions: BankTransaction[] = [];
    if (!cfg.disablePendingTransactions) {
        transactions.push(...pendingTransactions);
    }

    const postedTransactions = await getTransactionsFromTable('Transactions posted since last statement', true, processBorrowingTransactionRow);

    transactions.push(...postedTransactions);

    return transactions;
}
export async function getBankingTransactions(
    account: BankAccount,
) {
    const transactions = await getTransactionsFromTable('table summary', true, processBankingTransactionRow);

    return [
        ...transactions,
    ];
}


export async function getAccountInfo(
    account: BankAccount,
) {

    let latestTransactions: BankTransaction[] = [];
    await ensureURL(account.url, 'account activity', 1200);

    if (account.type === 'borrowing') {
        latestTransactions = await getBorrowingTransactions(account);
    } else if (account.type === 'banking') {
        latestTransactions = await getBankingTransactions(account);
    } else {
        throw new Error(`Not supported`);
    }

    return {
        account,
        latestTransactions,
    } as AccountInfo;

}


export function bankTXtoYNABTX(
    account: Account,
    bankTX: BankTransaction,
    importId: string,
) {
    return {
        account_id: account.id,
        amount: Math.round(bankTX.amount * 1000),
        date: bankTX.date.format(YNAB_DATE_FORMAT),
        cleared: bankTX.cleared ? SaveTransaction.ClearedEnum.Cleared : SaveTransaction.ClearedEnum.Uncleared,
        import_id: importId,
        payee_name: bankTX.description,
    };
}

export async function runApp2() {

    // logger.error(`test`, {asdfasfasf: 'adfadf'});
}

export async function sync() {

    await ensureURL(accountSummary);
    const { data: { accounts: ynabAccounts } } = await ynab.accounts.getAccounts(budget.id);
    const bankAccounts = await getAllAccounts();

    const ynabUpdateTransactions: UpdateTransaction[] = [];
    const ynabCreateTransactions: SaveTransaction[] = [];

    for (const ynabAccount of ynabAccounts) {
        const bankAccount = await getAccountLink(ynabAccount, bankAccounts);

        if (!bankAccount) { continue; }

        await ensureURL(accountSummary);
        logger.info(`Processing bank account ${ bankAccount.name } (${ bankAccount.balance }) to YNAB ${ ynabAccount.name } (${ ynabAccount.balance / 1000 })`);

        const accountInfo = await getAccountInfo(bankAccount);
        const [lastTx] = accountInfo.latestTransactions.slice(-1);

        // no transactions to update
        if (!lastTx) { continue; }

        if (!ynabAccount) { continue; }

        const { data: { transactions: ynabTransactions } } = await ynab.transactions.getTransactionsByAccount(budget.id, ynabAccount.id, lastTx.date.toDate());
        // logger.info(ynabTransactions);

        // logger.info(accountInfo);
        // logger.info(ynabAccount);

        // const transactions = {};
        const transactionMap: {
            [importId: string]: {
                ynabTX?: TransactionDetail;
                bankTX?: BankTransaction;
            };
        } = {};

        // const ynabTransactionMap: { [ key: string ]: boolean } = {};
        for (const ynabTransaction of ynabTransactions) {

            // const date = dayjs(ynabTransaction.date, 'YYYY-MM-DD')
            if (!ynabTransaction.import_id) { continue; }
            transactionMap[ynabTransaction.import_id] = transactionMap[ynabTransaction.import_id] || {};
            transactionMap[ynabTransaction.import_id].ynabTX = ynabTransaction;
        }

        for (const bankTransaction of accountInfo.latestTransactions) {
            if (!bankTransaction.amount) {
                logger.warn(`Falsey bank amount:`, bankTransaction);
                continue;
            }
            let importId: string;
            let index = 1;
            do {
                importId = [
                    `YNAB`,
                    Math.round(bankTransaction.amount * 1000),
                    bankTransaction.date.format(YNAB_DATE_FORMAT),
                    index,
                ].join(':');
                index++;

            } while (transactionMap[importId]?.bankTX);
            transactionMap[importId] = transactionMap[importId] || {};
            transactionMap[importId].bankTX = bankTransaction;
        }

        let matched = 0;
        let nBank = 0;
        let nYnab = 0;
        for (const [importId, { bankTX, ynabTX }] of Object.entries(transactionMap)) {

            if (bankTX && ynabTX) {
                // logger.info(`${ importId }  -  ${ bankTX.description }  - ${ ynabTX.payee_name }`);
                matched++;
            } else {
                // logger.info(`No match: ${ importId }  -  ${ bank?.description }  - ${ ynab?.payee_name }`);
            }

            if (bankTX) {
                nBank++;
            }
            if (ynabTX) {
                nYnab++;
            }

            if (bankTX && ynabTX && ynabTX.approved) { continue; }
            // do actions here

            // update to cleared
            if (bankTX && ynabTX && bankTX.cleared && !ynabTX.cleared) {
                logger.info(`UPDATE ${importId} on ${ ynabAccount.name }:`, ynabTX);
                ynabUpdateTransactions.push({
                    id: ynabTX.id,
                    ...bankTXtoYNABTX(ynabAccount, bankTX, importId),
                });
            }

            if (bankTX && !ynabTX) {
                // const description = bankTX.description.split('\n').join(' ');
                logger.info(`CREATE ${importId} on  ${ ynabAccount.name }:`, bankTX);
                ynabCreateTransactions.push({
                    ...bankTXtoYNABTX(ynabAccount, bankTX, importId),
                });
                // logger.info(`YNAB transaction created: ${ importId }`, transaction);
                // throw new Error(`Transaction CREATED!!!`);
            }
        }

        logger.info(`MATCHED: ${matched} ${nBank} ${nYnab}`);
    }

    if (cfg.dry) {
        logger.info(`DRY Sync complete.`);
        return;
    }

    if (ynabCreateTransactions?.length) {

        const { data: { transactions, duplicate_import_ids } } = await ynab.transactions.createTransactions(budget.id, {
            transactions: ynabCreateTransactions,
        });
        logger.info(`${transactions?.length} transactions created.`);
        if (duplicate_import_ids?.length) {
            logger.warn(`Duplicate import ids:`, duplicate_import_ids);
        }
    }

    if (ynabUpdateTransactions?.length) {
        const { data: { transactions, duplicate_import_ids } } = await ynab.transactions.updateTransactions(budget.id, {
            transactions: ynabUpdateTransactions,
        });

        logger.info(`${transactions?.length} transactions updated.`);
        if (duplicate_import_ids?.length) {
            logger.warn(`Duplicate import ids:`, duplicate_import_ids);
        }
    }
    logger.info(`Sync complete.`);
}

export async function runInterval(force = false) {

    const now = dayjs();

    // only run in sync hours
    if (now.isAfter(dayjs().set('hours', cfg.maxSyncHours)) && !force) {
        return;
    }
    if (now.isBefore(dayjs().set('hours', cfg.minSyncHours)) && !force) {
        return;
    }

    await initSelenium();
    await sync();
    await quitSelenium();
}

let interval: NodeJS.Timer | undefined;
export async function runApp() {

    const duration = dayjs.duration(cfg.interval, 'ms');
    logger.info(`Running sync now and every ${duration.humanize()}`);
    await runInterval(true);

    return new Promise(() => {
        // we never need to call resolve...
        interval = setInterval(() => {
            logger.info(`Running interval...`);
            runInterval().then(() => {
                logger.info(`Interval completed successfully`);
            }).catch((err) => {
                logger.error(`Error running interval:`, err);
                // TODO: push message somewhere if this is erroring out
            });
        }, cfg.interval);
    });

}

export async function stopApp() {
    if (interval) {
        clearInterval(interval);
        interval = undefined;
    }
    // TODO: wait for not running
}