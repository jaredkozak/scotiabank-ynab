import './dayjs';

import { runApp, stopApp } from './app';
import { initConfig } from './config';
import { initLogger, logger } from './logger';
import { initSelenium, quitSelenium } from './selenium';
import { initYNAB } from './ynab';

export async function start() {
    initConfig();
    initLogger();
    await initYNAB();

    // await initSelenium();
    await runApp();
}


start().then(async () => {
    logger.info(`Completed.`);
    await stop();
    process.exit(0);
}).catch((err) => {
    logger.error(`Fatal error:`, err);
    tryStop();
});

export async function stop() {
    await stopApp();
    await quitSelenium();
}

export function tryStop(exitCode?: number) {
    stop().catch((stopError) => {
        logger.error(`Fatal error while stopping:`, stopError);
    }).then(() => {
        if (exitCode !== undefined) {
            process.exit(exitCode);
        }
    });
}

process.on('SIGTERM', () => {
    logger.info(`Got SIGTERM`);
    tryStop(0);
});
